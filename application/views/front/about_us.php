<div class="container-fluid">
    <div id="heading" class="row">
        <div class="col-12">
            <header>
                <h1 class="hidden-xs">About Us</h1>
                <h6 class="visible-xs">About Us</h6>
                <h2>Know more</h2>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <article class="inner">
                <div class="row">
                    <div class="col-12">
                        <?php echo $page->description ?>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
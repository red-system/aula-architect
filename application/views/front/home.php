<!-- <div id="fullscreen-slider" class="swiper-container">
    <div class="swiper-wrapper">
        <?php foreach($slider as $item): ?>
            <div class="swiper-slide" >
                <h1 class="text-<?php echo $item->align ?>"><?php //echo $item->title ?></h1>
            </div>
        <?php endforeach ?>
    </div>
</div> -->

<div id="hero-wrapper">
    <div class="carousel-wrapper">
        <div id="hero-carousel" class="carousel slide carousel-fade">
            <div class="carousel-inner">
                <?php $i = 0; ?>
                <?php foreach($slider as $item): ?>
                    <?php ($i == 0) ? $status = 'active' : $status = '' ?>
                <div class="item <?= $status ?>">
                    <img src="<?php echo site_url('upload/images/') . $item->thumbnail ?>">
                </div>
                <?php $i++ ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>